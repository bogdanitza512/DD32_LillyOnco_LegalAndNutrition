﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class ContactViewManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    string currentEmail;

    [SerializeField]
    VirtualKeyboard vk = new VirtualKeyboard();


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void OnInputField_Select()
    {
        vk.ShowTouchKeyboard();
    }

    public void OnInputField_Deselect()
    {
        vk.HideTouchKeyboard();
    }

    public void OnInputField_EndEdit(string email)
    {
        currentEmail = email;
    }

    public void OnSubmitButton_Click()
    {
        // send request here
    }


    #endregion

}
