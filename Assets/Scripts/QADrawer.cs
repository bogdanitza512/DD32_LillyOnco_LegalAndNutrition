﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System;
using Doozy.Engine.UI;


/// <summary>
/// 
/// </summary>
public class QADrawer : SerializedMonoBehaviour {

    #region Nested Types
        
	public enum SelectState { Selected, Unselected}
    public enum DropdownState { Open, Closed }

    public struct SuperState
    {
        public SelectState selectState;
        public DropdownState dropdownState;
    }

    public struct Specs
    {
        public Color headerColor;
        public Color textColor;
        public Color dropdownColor;
        public Color contentColor;
        public Vector3 contentPosition;
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    public QAPanel panel;

    [SerializeField]
    Image header;

    [SerializeField]
    TMP_Text text;

    [SerializeField]
    Image dropdown;

    [SerializeField]
    Image content;

    [SerializeField]
    float duration;

    [SerializeField]
    Dictionary<SuperState, Specs> stateToSpec =
        new Dictionary<SuperState, Specs>();

    [SerializeField]
    SuperState currentState;

    [SerializeField]
    GameObject drawerContent;

    [SerializeField]
    UIToggle dropdownToggle;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void OnSelectToggleValueChanged(bool newValue)
    {
        var newState = currentState;

        newState.selectState =
            newValue ?
                SelectState.Selected :
                SelectState.Unselected;

        panel.MutateSelection(this, newValue);
        
        TransitionTo(newState);
    }

    public void OnDropdownToggleValueChanged(bool newValue)
    {
        if(newValue)
        {
            drawerContent.SetActive(true);
            panel.OpenDetails(this);
        }
        else
        {
            drawerContent.SetActive(false);
            panel.CloseDetails(this);
        }
    }


    public void SetDropdownState(bool newValue)
    {
        var newState = currentState;

        newState.dropdownState =
            newValue ?
                DropdownState.Open :
                DropdownState.Closed;

        TransitionTo(newState);
    }

    public void TransitionTo(SuperState newState, TweenCallback onCompleteCallback = null)
    {
        DOTween
            .Sequence()
            .Append(
                header
                    .DOColor(
                        stateToSpec[newState].headerColor,
                        duration
                    )
            )
            .Join(
                text
                    .DOColor(
                        stateToSpec[newState].textColor,
                        duration
                    )
            )
            .Join(
                dropdown
                    .DOColor(
                        stateToSpec[newState].dropdownColor,
                        duration
                    )
            )
            .Join(
                content
                    .DOColor(
                        stateToSpec[newState].contentColor,
                        duration
                    )
            )
            .Join(
                content
                    .rectTransform
                    .DOLocalMove(
                        stateToSpec[newState].contentPosition,
                        duration
                    )
            )
            .OnComplete(onCompleteCallback);

        currentState = newState;
    }

    [Button(ButtonSizes.Medium)]
    public Vector3 GetMoveTransform(RectTransform subject)
    {
        return subject.localPosition;
    }

    public void SimulateCloseDrawer()
    {
        dropdownToggle.ToggleOff();
    }

    #endregion

}
