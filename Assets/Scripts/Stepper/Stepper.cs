﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using BN512.Extensions;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class Stepper : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    bool hasBeenPopulated;
    public bool HasBennPopulated => hasBeenPopulated;

    [SerializeField]
    List<StepperLine> lines =
        new List<StepperLine>();

    [SerializeField]
    List<StepperWaypoint> waypoints =
        new List<StepperWaypoint>();

    [SerializeField]
    int currentState;

    [SerializeField]
    List<UnityEvent> events =
        new List<UnityEvent>();


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (!hasBeenPopulated)
        {
            AutoPopulate();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulate()
    {
        if(hasBeenPopulated)
        {
            lines.Clear();
            waypoints.Clear();
        }

        foreach (Transform child in transform)
        {
            var line = child.GetComponent<StepperLine>();
            var waypoint = child.GetComponent<StepperWaypoint>();

            if (line != null)
            {
                lines.Add(line);
                if(!line.HasBennPopulated)
                {
                    line.AutoPopulate();
                }
                continue;
            }
            else if(waypoint != null)
            {
                waypoints.Add(waypoint);
                if (!waypoint.HasBennPopulated)
                {
                    waypoint.AutoPopulate();
                }
                continue;
            }
        }

        hasBeenPopulated = true;
    }

    public void TransitionTo(int newState)
    {
        if (newState < 0 || newState > waypoints.Count) return;

        //0:.1...2...3...4...5
        //0:...1...2...3...4...
        if (newState > currentState)
        {
            for (var i = currentState; i < newState; i++)
            {
                if (i == 0)
                {
                    waypoints[i]
                        .TransitionTo(StepperWaypoint.State.Active);
                }
                else
                {
                    var index = i - 1;

                    lines[index]
                        .TransitionTo(
                            StepperLine.State.Active,
                            () =>
                            {
                                waypoints[index + 1]
                                    .TransitionTo(StepperWaypoint.State.Active);
                            }
                        );
                }
            }
        }
        //0:.1...2...3...4...5.
        //0:...1...2...3...4...
        else if (newState < currentState)
        {
            for (var i = currentState; i > newState; i--)
            {
                if (i == 0)
                {
                    waypoints[i]
                        .TransitionTo(StepperWaypoint.State.Inactive);
                }
                else
                {
                    var index = i - 1;

                    waypoints[index]
                        .TransitionTo(
                            StepperWaypoint.State.Inactive,
                            () =>
                            {
                                if(index - 1 >= 0)
                                {
                                    lines[index - 1]
                                        .TransitionTo(StepperLine.State.Inactive);
                                }
                            }
                        );
                }
            }
        }

        currentState = newState;
        if(events.Count >= currentState)
        {
            events[currentState].Invoke();
        }
    }

    [Button(ButtonSizes.Medium)]
    public void Increment(int factor = 1)
    {
        TransitionTo(currentState + factor);
    }

    [Button(ButtonSizes.Medium)]
    public void Decrement(int factor = 1)
    {
        TransitionTo(currentState - factor);
    }

    #endregion

}
