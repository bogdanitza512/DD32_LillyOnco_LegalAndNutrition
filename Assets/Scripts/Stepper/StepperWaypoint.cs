﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class StepperWaypoint : SerializedMonoBehaviour {

    #region Nested Types

    public struct Spec
    {
        public float activeAlpha;
        public Color textColor;
        public float duration;
    }

    public enum State { Inactive, Active }

    #endregion

    #region Fields and Properties

    [HideInInspector]
    bool hasBeenPopulated;

    public bool HasBennPopulated => hasBeenPopulated;

    [SerializeField]
    Image active;

    [SerializeField]
    Image inactive;

    [SerializeField]
    TMP_Text label;

    [SerializeField]
    TMP_Text description;

    [SerializeField]
    Dictionary<State, Spec> stateToSpec =
        new Dictionary<State, Spec>();

    [SerializeField]
    State currentState;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (!hasBeenPopulated)
        {
            AutoPopulate();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulate()
    {
        foreach (Transform child in transform)
        {
            var image = child.GetComponent<Image>();
            var text = child.GetComponent<TMP_Text>();

            if (image != null)
            {
                if (child.name.Contains("Active"))
                {
                    active = image;
                }
                else if (child.name.Contains("Inactive"))
                {
                    inactive = image;
                }
            }
            else if(text != null)
            {
                if (child.name.Contains("Label"))
                {
                    label = text;
                }
                else if (child.name.Contains("Description"))
                {
                    description = text;
                }
            }
        }

        hasBeenPopulated = true;
    }

    public void TransitionTo(State newState, TweenCallback onCompleteCallback = null)
    {
        DOTween
            .Sequence()
            .Append(
                active
                    .DOFade(
                        stateToSpec[newState].activeAlpha,
                        stateToSpec[newState].duration
                    )
            )
            .Join(
                description
                    .DOColor(
                        stateToSpec[newState].textColor,
                        stateToSpec[newState].duration
                    )
            )
            .OnComplete(onCompleteCallback);

        currentState = newState;
    }

    #endregion

}
