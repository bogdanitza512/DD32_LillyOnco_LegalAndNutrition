﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Doozy.Engine.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class QAPanel : MonoBehaviour {

    #region Nested Types

    public enum State { Closed, Open }

    #endregion

    #region Fields and Properties

    [SerializeField]
    bool hasBeenPopulated;

    [SerializeField]
    RectTransform drawersRoot;

    [SerializeField]
    List<QADrawer> drawers =
        new List<QADrawer>();

    [SerializeField]
    CanvasGroup blur;

    [SerializeField]
    Image img;

    [SerializeField]
    RectTransform center;

    [SerializeField]
    State currentState;

    [SerializeField]
    Vector3 cachedPos;

    [SerializeField]
    QADrawer currentDrawer;

    [SerializeField]
    float duration;

    [SerializeField]
    UIButton submitButton;

    [SerializeField]
    Stepper stepper;

    [SerializeField]
    List<int> selection =
        new List<int>();

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (!hasBeenPopulated)
        {
            AutoPopulate();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulate()
    {
        if (hasBeenPopulated)
        {
            drawers.Clear();
        }

        foreach (Transform child in drawersRoot)
        {
            var drawer = child.GetComponent<QADrawer>();

            if (drawer != null)
            {
                drawers.Add(drawer);
                drawer.panel = this;
                continue;
            }
        }

        hasBeenPopulated = true;
    }

    public void MutateSelection(QADrawer drawer, bool newValue)
    {
        int index = drawers.IndexOf(drawer);
        if (newValue)
        {
            if(!selection.Contains(index))
            {
                selection.Add(index);
            }
        }
        else
        {
            if(selection.Contains(index))
            {
                selection.Remove(index);
            }
        }

        if(selection.Count == 0)
        {
            stepper.TransitionTo(1);
        }
        else
        {
            stepper.TransitionTo(2);
        }
    }

    public void OpenDetails(QADrawer drawer)
    {
        if(currentState == State.Closed)
        {
            cachedPos = drawer.transform.position;
            currentDrawer = drawer;
            img.raycastTarget = true;

            DOTween
                .Sequence()
                .OnStart(
                    ()=>
                    {
                        drawer.transform.SetParent(blur.transform);
                        blur.gameObject.SetActive(true);
                    }
                )
                .Append(
                    blur.DOFade(1, duration)
                    )
                .Join(
                    drawer
                        .transform
                        .DOMove(
                            center.position,
                            duration
                        )
                    )
                .OnComplete(()=> { drawer.SetDropdownState(true); });

            currentState = State.Open;
        }
    }

    public void CloseDetails(QADrawer drawer)
    {
        if(currentState == State.Open)
        {
            img.raycastTarget = false;
            currentDrawer = null;


            DOTween
                .Sequence()
                .OnStart(
                    () =>
                    {
                        drawer.transform.SetParent(drawersRoot);
                        drawer.SetDropdownState(false);
                        blur.gameObject.SetActive(false);
                    }
                )
                .Append(
                    blur.DOFade(0, duration)
                    )
                .Join(
                    drawer
                        .transform
                        .DOMove(
                            cachedPos,
                            duration
                        )
                    )
                .OnComplete(() => {  });

            currentState = State.Closed;
        }

    }

    public void SimulateCloseDrawer()
    {
        if(currentDrawer)
        {
            currentDrawer.SimulateCloseDrawer();
        }
    }

    public void MakeSubmitButtonInteractable()
    {
        submitButton.Interactable = true;
    }

    public void MakeSubmitButtonNotInteractable()
    {
        submitButton.Interactable = false;
    }

    #endregion

}
