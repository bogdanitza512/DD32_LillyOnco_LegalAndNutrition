﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class DropdownToggle : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    Image targetSprite;

    [SerializeField]
    float duration = 0.5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnToggleValueChanged(bool newValue)
    {
        var targetRect = targetSprite.rectTransform;

        if(DOTween.IsTweening(targetRect))
        {
            targetRect.DOComplete();
        }

        var rotAmount =
            newValue ? 180 : 0;

        var newRot =
            targetRect.rotation.eulerAngles.WithZ(rotAmount);

        targetRect.DORotate(
            newRot,
            duration);
    }

    #endregion

}
